import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class MasterMindKtTest {

    // isColorGuessCorrect()

    @Test
    fun isRedInRandomColors() {
        val expected = true
        assertEquals(expected, isColorGuessCorrect(0, mutableListOf("red","blue","green","purple"), mutableListOf("blue","red","purple","purple")))
    }

    @Test
    fun isYellowInRandomColors() {
        val expected = false
        assertEquals(expected, isColorGuessCorrect(1, mutableListOf("blue","yellow","green","purple"), mutableListOf("blue","red","purple","purple")))
    }

    // isGuessCorrect()

    @Test
    fun isBlueInRandomColorsAndHasCorrectPosition() {
        val expected = true
        assertEquals(expected, isGuessCorrect(0, mutableListOf("blue","yellow","green","purple"), mutableListOf("blue","red","purple","purple")))
    }

    @Test
    fun isGreenInRandomColorsAndHasCorrectPosition() {
        val expected = false
        assertEquals(expected, isGuessCorrect(2, mutableListOf("blue","yellow","green","purple"), mutableListOf("blue","green","purple","purple")))
    }

    // doesColorExist()

    @Test
    fun doesPurpleColorExist() {
        val expected = true
        assertEquals(expected, doesColorExist("purple", listOf("red","blue","green","yellow","purple")))
    }

    @Test
    fun doesBrownColorExist() {
        val expected = false
        assertEquals(expected, doesColorExist("brown", listOf("red","blue","green","yellow","purple")))
    }

    // simpleLine()

    @Test
    fun printSimpleLine() {
        val expected = "———————————————————————————————————————————————"
        assertEquals(expected, simpleLine())
    }

    // shortLine()

    @Test
    fun printShortLine() {
        val expected = "—————————————————————————————"
        assertEquals(expected, shortLine())
    }

    // nameOfGame()

    @Test
    fun printNameOfGame() {
        val expected = "\n\uD83D\uDCA5 Mastermind Console Game \uD83D\uDCA5"
        assertEquals(expected, nameOfGame())
    }

    // rulesOfGame()

    @Test
    fun printRulesOfGame() {
        var expected = ""
        expected += ("Rules \uD83D\uDCC3\n")
        expected += ("\n1. The computer picks a sequence of colors. The number of colors is 4.")
        expected += ("\n2. The objective of the game is to guess the exact positions of the colors in the computer's sequence.")
        expected += ("\n3. A color can be used only once in a code sequence")
        expected += ("\n4. After filling a line with your guesses, the computer responses with the result of your guess.")
        expected += ("\n5. For each color in your guess that is in the correct color and correct position in the code sequence, the computer display a \uD83C\uDFF4 on the left side of the current guess.")
        expected += ("\n6. For each color in your guess that is the correct color but is NOT in the correct position in the code sequence, the computer display a \uD83C\uDFF3 on the left side of the current guess.")
        expected += ("\n7. You win the game when you manage to guess all the colors in the code sequence and when they all in the right position.")
        expected += ("\n8. You lose the game if you use all 6 attempts without guessing the computer code sequence.")
        assertEquals(expected, rulesOfGame())
    }
}