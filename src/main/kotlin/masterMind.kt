import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*
import kotlin.system.exitProcess

/*
    * AUTHOR: Erik Vasilyan
    * TITLE: Mastermind
    * DESCRIPTION: This program is a version of the famous MasterMind that works through the console.
*/

const val white = "\uD83C\uDFF3"
const val black = "\uD83C\uDFF4"
const val line = "—"
const val red = "\uD83D\uDD34"
const val blue = "\uD83D\uDD35"
const val green = "\uD83D\uDFE2"
const val yellow = "\uD83D\uDFE1"
const val purple = "\uD83D\uDFE3"
const val brown = "\uD83D\uDFE4"
const val blackColor = ""
const val colorWhiteSpace = "\uD83D\uDD18"
const val guessWhiteSpace = "\uD83C\uDFC1"
const val nothingSymbol = "  "
var playAgain = true
var gameMode = ""
var userName = ""
var gameHistoryFile = File("gameHistory.txt")

/**
 * Main function of the program from where starts the game
 * @author Erik Vasilyan
 * @since 1.0.0
 */
fun main() {
    val scanner = Scanner(System.`in`)
    var attempts = 0

    println(nameOfGame())
    println(shortLine())

    println("Menu")
    showPrincipalMenu(scanner)

    while (playAgain) {
        println(simpleLine())

        var colors = listOf<String>()
        when (gameMode) {
            "Classic" -> colors = listOf("red","blue","green","yellow","purple")
            "777" -> colors = listOf("red","blue","green","yellow","purple","brown","orange")
        }

        val colorsToGuess = generateRandomColors(colors)
        val attemptsHistory = mutableListOf<List<List<String>>>()
        when (gameMode) {
            "Classic" -> { attempts = 6 }
            "777" -> { attempts = 7 }
        }
        var win = false
        var correctGuesses = 0
        var correctColorGuesses = 0
        var nothing = 0
        var totalBlackFlags = 0
        var totalWhiteFlags = 0

        for (attempt in 1..attempts) {
            val userColors = mutableListOf<String>()
            val allUserColors = mutableListOf<List<String>>()
            val allGuesses = mutableListOf<List<String>>()
            val guesses = mutableListOf<String>()

            addUserColors(colors, userColors, allUserColors, colorsToGuess, scanner)

            val copyOfUserColors = mutableListOf<String>()
            for (elem in userColors) copyOfUserColors.add(elem)

            val copyOfGuessColors = mutableListOf<String>()
            for (elem in colorsToGuess) copyOfGuessColors.add(elem)

            // Remove all colors that have black flags from copy lists to have a list only of white colors and nothing's
            for (m in userColors.indices) {
                if (isGuessCorrect(m, userColors, colorsToGuess)) {
                    correctGuesses++
                    copyOfGuessColors.remove(userColors[m])
                    copyOfUserColors.remove(userColors[m])
                }
            }

            for (userColor in copyOfUserColors) {
                if (userColor in copyOfGuessColors) correctColorGuesses++
                else nothing++
            }

            for (m in userColors.indices) changeColorTextToSymbol(m, userColors)

            if (correctGuesses == colorsToGuess.size) {
                for (n in 1..correctGuesses) guesses.add(black)
                win = true
            }
            else {
                for (n in 1..correctGuesses) {
                    guesses.add(black)
                    totalBlackFlags++
                }
                for (n in 1..correctColorGuesses) {
                    guesses.add(white)
                    totalWhiteFlags++
                }
                for (n in 1..nothing) guesses.add(nothingSymbol)
            }

            addAttemptToHistory(allGuesses, guesses, attemptsHistory, allUserColors)
            println(simpleLine())
            printFutureAttempts(attempts, attemptsHistory, gameMode)
            printHistoryOfAttempts(attemptsHistory)
            println(simpleLine())

            if (win) break

            correctGuesses = 0
            correctColorGuesses = 0
            nothing = 0
        }

        val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
        val formattedDate = LocalDateTime.now().format(formatter)

        if (win) gameHistoryFile.appendText("$formattedDate -> $userName has won the game with $attempts attempts\n")
        else gameHistoryFile.appendText("$formattedDate -> $userName lose the game and got $totalBlackFlags black flags and $totalWhiteFlags white flags\n")

        printGameResult(win)
        println(simpleLine())
        askUserToPlayAgain(scanner)
    }

    println(simpleLine())
    println("See you soon!")
}

/**
 * Shows the menu of modes that user can pick to play
 * @since 1.0.0
 */
fun showGameModeMenu() {
    println("1. Classic" +
            "\n2. 777" +
            "\n3. Return"
    )
}

/**
 * Shows the principal menu of the game from where user can choose options
 * @since 1.0.0
 */
fun showPrincipalMenu(scanner: Scanner) {
    println(shortLine())
    println("1. Play" +
            "\n2. Rules" +
            "\n3. History" +
            "\n4. Quit")

    var selectedOption = askUserToChooseOptionFromMenu(4, scanner)
    println(shortLine())

    when (selectedOption) {
        1 -> {
            playAgain = true
            askUserName(scanner)
            println(shortLine())
            showGameModeMenu()
            selectedOption = askUserToChooseOptionFromMenu(3, scanner)
            when (selectedOption) {
                1 -> { gameMode = "Classic" }
                2 -> { gameMode = "777" }
                3 -> { showPrincipalMenu(scanner) }
            }
        }
        2 -> {
            simpleLine()
            println(rulesOfGame())
            showPrincipalMenu(scanner)
        }
        3 -> {
            gameHistoryFile.forEachLine { println(it) }
            showPrincipalMenu(scanner)

        }
        4 -> { exitProcess(0) }
    }
}

/**
 * Asks user to insert a name and saves it.
 * @param scanner Scanner
 */
fun askUserName(scanner: Scanner) {
    print("Your name: ")
    userName = scanner.next()
}

/**
 * Asks user to choose an option from the menu
 * @param nOfOptions Number of options that user can choose
 * @param scanner Scanner
 * @return Int that is a selected option
 */
fun askUserToChooseOptionFromMenu(nOfOptions: Int, scanner: Scanner): Int {
    var selectedOption: Int
    do {
        print("\nOption: ")
        selectedOption = scanner.nextInt()
        if (selectedOption !in 1..nOfOptions) {
            println("$selectedOption it's not valid option!")
        }
    } while (selectedOption !in 1..nOfOptions)

    return selectedOption
}

/**
 * Function that changes text colors like "red" to symbol/emoji
 * @param m Current index of color in the list
 * @param userColors Mutable List of user colors
 */
fun changeColorTextToSymbol(m: Int, userColors: MutableList<String>) {
    when (userColors[m]) {
        "red" -> userColors[m] = red
        "blue" -> userColors[m] = blue
        "green" -> userColors[m] = green
        "yellow" -> userColors[m] = yellow
        "purple" -> userColors[m] = purple
        "brown" -> userColors[m] = brown
        "orange" -> userColors[m] = blackColor
    }
}

/**
 * Function that checks if the user color is in the list of random colors
 * @param m Current index of color in the list
 * @param userColors Mutable List of user colors
 * @param colorsToGuess Mutable List of random colors that user needs to guess
 * @return Returns "true" if user color is in random colors or "false" if it's not
 */
fun isColorGuessCorrect(m: Int, userColors: MutableList<String>, colorsToGuess: MutableList<String>): Boolean {
    return userColors[m] in colorsToGuess && userColors[m] != colorsToGuess[m]
}

/**
 * Function that checks if the user color is in the list of random colors and is in correct position
 * @param m Current index of color in the list
 * @param userColors Mutable List of user colors
 * @param colorsToGuess Mutable List of random colors that user needs to guess
 * @return Returns "true" if user color is in random colors and has the correct position or "false" if it's not
 */
fun isGuessCorrect(m: Int, userColors: MutableList<String>, colorsToGuess: MutableList<String>): Boolean {
    return userColors[m] == colorsToGuess[m]
}

/**
 * Function that asks user to input colors and adds it to mutable list
 * @param userColors Mutable List of user colors
 * @param colorsToGuess Mutable List of random colors that user needs to guess
 * @param scanner Scanner
 * @param allUserColors Mutable List that saves all user colors of the attempt
 * @param colors List of all colors allowed in the game
 */
fun addUserColors(colors: List<String>, userColors: MutableList<String>, allUserColors: MutableList<List<String>>, colorsToGuess: MutableList<String>, scanner: Scanner) {
    println(chooseColors(colors))
    for (j in colorsToGuess.indices) {
        print("Color ${j + 1}: ")
        var userInputColor = scanner.next().lowercase()

        while (!(doesColorExist(userInputColor, colors))) {
            print("$userInputColor it's not a valid color!")
            println(chooseColors(colors))
            print("Color ${j + 1}: ")
            userInputColor = scanner.next().lowercase()
        }
        userColors.add(userInputColor)
    }
    allUserColors.add(userColors)
}

/**
 * Function that prints game result, Win or Lose
 * @param win Boolean that saves the result of the game, "true" if win or "false" if lose
 */
fun printGameResult(win: Boolean) {
    if (win) println("\uD83C\uDF89 You Win! \uD83C\uDF89")
    else println("\uD83D\uDC7F You Lose! \uD83D\uDC7F")
}

/**
 * Function that asks user to play again the game
 * @param scanner Scanner allows user to input
 */
fun askUserToPlayAgain(scanner: Scanner) {
    var userAnswer: String
    do {
        print("Do you want to play again [Y / N]: ")
        userAnswer = scanner.next().uppercase()

    } while (userAnswer != "Y" && userAnswer != "N")

    if (userAnswer == "N") {
        playAgain = false
        showPrincipalMenu(scanner)
    }
}

/**
 * Function that prints simple line
 * @return String that is a simple line
 */
fun simpleLine(): String {
    var sequence = ""
    repeat(47) { sequence += line }
    return sequence
}

/**
 * Function that prints history of the attempts played
 * @param attemptsHistory Mutable List that saves all the attempts played
 */
fun printHistoryOfAttempts(attemptsHistory: MutableList<List<List<String>>>) {
    for (i in attemptsHistory.size-1 downTo 0) {
        print("${i+1}  ")
        print("${attemptsHistory[i][0].toString().replace(",", "").replace("[", "").replace("]", " ")} ")
        print(attemptsHistory[i][1].toString().replace(",", "").replace("[", "").replace("]", ""))
        println("")
    }
}

/**
 * Function that prints future attempts that user didn't played yet
 * @param attempts Int that saves number of attempts that has the user
 * @param attemptsHistory Mutable List that saves all the attempts played
 */
fun printFutureAttempts(attempts: Int, attemptsHistory: MutableList<List<List<String>>>, gameMode: String) {

    var nOfGuessSpaces = 0
    when (gameMode) {
        "Classic" -> { nOfGuessSpaces = 4 }
        "777" -> { nOfGuessSpaces = 7 }
    }

    for (p in attempts-attemptsHistory.size downTo 1) {
        print("${attemptsHistory.size+p}  ")
        repeat(nOfGuessSpaces) { print("$guessWhiteSpace ") }
        print(" ")
        repeat(nOfGuessSpaces) { print("$colorWhiteSpace ") }
        println("")
    }
}

/**
 * Function that adds finished attempt to the history of the game
 * @param allGuesses Mutable List that saves all guesses of the game
 * @param guesses Mutable List that saves all guess of the attempt
 * @param attemptsHistory Mutable List that saves all the attempts played
 * @param allUserColors Mutable List that saves all user colors of the attempt
 */
fun addAttemptToHistory(allGuesses: MutableList<List<String>>, guesses: MutableList<String>, attemptsHistory: MutableList<List<List<String>>>, allUserColors: MutableList<List<String>>) {
    allGuesses.add(guesses)
    attemptsHistory.add(allGuesses + allUserColors)
}

/**
 * Function that checks if user color is in the list of colors allowed in the game
 * @param userInputColor String that saves current input of user
 * @param colors List of all colors allowed in the game
 */
fun doesColorExist(userInputColor: String, colors: List<String>): Boolean {
    return userInputColor in colors
}

/**
 * Function that prints all colors to choose for user
 * @param colors List of all colors allowed in the game
 */
fun chooseColors(colors: List<String>): String {
    var colorsToChoose = ""
    colorsToChoose += ("Colors: ")
    for(color in colors) {
        when (color) {
            "red" -> colorsToChoose += ("$red$color ")
            "blue" -> colorsToChoose += ("$blue$color ")
            "green" -> colorsToChoose += ("$green$color ")
            "yellow" -> colorsToChoose += ("$yellow$color ")
            "purple" -> colorsToChoose += ("$purple$color ")
            "brown" -> colorsToChoose += ("$brown$color ")
            "orange" -> colorsToChoose += ("$brown$color ")
        }
    }
    return colorsToChoose
}

/**
 * Function that generates random 4 colors for the game
 * @param colors List of all colors allowed in the game
 * @return MutableList of random colors
 */
fun generateRandomColors(colors: List<String>): MutableList<String> {
    val randomColors = mutableListOf<String>()

    var nOfRandomColors = 0
    when (gameMode) {
        "Classic" -> { nOfRandomColors = 4 }
        "777" -> { nOfRandomColors = 7 }
    }

    repeat(nOfRandomColors) {
        val randomColor = colors.random()
        randomColors.add(randomColor)
    }
    return randomColors
}

/**
 * Function that returns all rules of the game for user
 * @return String that is all rules of game
 */
fun rulesOfGame(): String {
    var rules = ""
    rules += ("Rules \uD83D\uDCC3\n")
    rules += ("\n1. The computer picks a sequence of colors. The number of colors is 4.")
    rules += ("\n2. The objective of the game is to guess the exact positions of the colors in the computer's sequence.")
    rules += ("\n3. A color can be used only once in a code sequence")
    rules += ("\n4. After filling a line with your guesses, the computer responses with the result of your guess.")
    rules += ("\n5. For each color in your guess that is in the correct color and correct position in the code sequence, the computer display a \uD83C\uDFF4 on the left side of the current guess.")
    rules += ("\n6. For each color in your guess that is the correct color but is NOT in the correct position in the code sequence, the computer display a \uD83C\uDFF3 on the left side of the current guess.")
    rules += ("\n7. You win the game when you manage to guess all the colors in the code sequence and when they all in the right position.")
    rules += ("\n8. You lose the game if you use all 6 attempts without guessing the computer code sequence.")
    return rules
}

/**
 * Function that returns name of the game
 * @return String that is name of  the game
 */
fun nameOfGame(): String {
    return "\n\uD83D\uDCA5 Mastermind Console Game \uD83D\uDCA5"
}

/**
 * Function that returns a short line of symbols
 * @return String that is a short line
 */
fun shortLine(): String {
    var shortLine = ""
    repeat(29) { shortLine += line }
    return shortLine
}