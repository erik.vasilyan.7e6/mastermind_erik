Contents
========

 * [Title](#title)
 * [Description](#description)
 * [Installation](#installation)
 * [Usage](#usage)
 * [Configuration](#configuration)
 * [How to play?](#how-to-play)
 * [Want to contribute?](#want-to-contribute)

### Title
---

This program is a version of the famous MasterMind that works through the console.

The console games includes all the futures that have the original game.

### Description
---

This program is a pattern of Mastermind game that is a code-breaking game for two players. The second player in the program is the computer.

The console games includes:

+ Text
+ Colors
+ Emojis

### Installation
---

#### Install From Source:

```bash
$ git clone https://gitlab.com/erik.vasilyan.7e6/mastermind_erik.git
```

### Usage
---

To start the program, simply run the `masterMind.kt` file.

`masterMind.kt` isn't supported with command line arguments.

### Configuration
---

For the moment the only configuration you can do that is:

#### Change attempts value
---

If you'd like to modify the number of itemmpts that you have, you need to edit the `masterMind.kt` files `attempts` variable value.

For the default the attempts value is 6.

### How to play?
----

To start the game first check the [Usage](#usage) and run the program.

#### Rules:

* The computer picks a sequence of colors. The number of colors is 4.
* The objective of the game is to guess the exact positions of the colors in the computer's sequence.
* A color can be used only once in a code sequence
* After filling a line with your guesses, the computer responses with the result of your guess.
* For each color in your guess that is in the correct color and correct position in the code sequence, the computer display a black flag on the left side of the current guess.
* For each color in your guess that is in the correct color but is NOT in the correct position in the code sequence, the computer display a white flag on the left side of the current guess.
* You win the game when you manage to guess all the colors in the code sequence and when they all in the right position.
* You lose the game if you use all 6 (by default) attempts without guessing the computer code sequence.

#### Start of the game:

* Computer already have the 4 colors sequence and you need to start to guess thats colors
* In the console you see all colors that you can use and every time you use a color, that color you can't use more in that attempt, the game will you know about the colors that you can use.
* After you insert all 4 colors for the attempt, the program will you know about the correct color/position guesses with black and white flags in the left side of the colors.
* If you got 4 black flags you win, if no't you pass to next attempt to guess the colors in sequence for every attempt (by default are 6).

### Want to Contribute?
---

Check out `LICENSE` file
